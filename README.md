# microservices
## Development
Please follow the instruction below to run microservices

Overview
1. Microservices model: https://www.jhipster.tech/api-gateway/
2. JHipster installation: https://www.jhipster.tech/installation/
3. Generate microservice: https://www.jhipster.tech/creating-an-app/

Steps to run microservices:
1. Clone JHipster Registry: https://github.com/jhipster/jhipster-registry.git
    
    cd jhipster-registry
    
    run cmd: ./mvnw (Linux/MacOS)
2. Create uaa service(available)
    
    cd uaa
    
    run cmd: ./mvnw (Linux/MacOS)
3. Create gateway(available)
    
    cd gateway
    
    run cmd: ./mvnw (Linux/MacOS)
4. Create service(available - greeting)
    
    cd greeting
    
    run cmd: ./mvnw (Linux/MacOS)

