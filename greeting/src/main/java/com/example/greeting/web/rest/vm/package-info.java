/**
 * View Models used by Spring MVC REST controllers.
 */
package com.example.greeting.web.rest.vm;
