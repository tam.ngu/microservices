package com.example.greeting.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class GreetingResource {

    @GetMapping("/api/greeting")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public ResponseEntity<String> greeting() {
        log.info("Greeting from micro-service");
        return ResponseEntity.status(HttpStatus.OK)
            .body("Greeting from micro-service");
    }
}
